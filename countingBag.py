#Counting bag ADT

from linearbag import Bag
import os

#CountingBag class
class CountingBag(Bag):
    #constructs a Counting bag 
    def __init__( self ):
        super().__init__()

    #Retunrs the number of a given item
    def numOf( self , item ):
        #counter
        cpt = 0
        if item == None or len(self.get_items()) <= 0:
             return cpt
        else:
            for it in self.get_items():
                if it == item:
                    cpt += 1
        return cpt



def main():
    os.chdir("D:\\python-workspace\\algorithms")
    cBag = CountingBag()
    cBag.add(1)
    cBag.add(2)
    cBag.add(0)
    cBag.add(2)
    print(f"The number of 2 is {cBag.numOf(2)}")

if __name__ == "__main__":
    main()