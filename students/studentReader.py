# @author raphael keita
#The student file reader that allows to get records from a source

version = "0.1"
class StudentFileReader():
    #constructs a fileReader
    def __init__( self, filename , filetype):
        self._filename = filename
        self._inSource = None
        self._separator = filetype

    #opens a connection to the input source
    def open( self ):
        try:
            self._inSource = open( self._filename, "r")
        except Exception as e:
            raise ConnectionError (f'{e}')
    
    #closes a connection to the input source
    def close( self ):
        try:
            self._inSource.close()
            self._inSource = None
        except:
            self._inSource = None

    #Extracts the next student record from the input source
    def fetchRecord( self ):   
        #an input source is not initialized 
        if not self._inSource:
            raise ('No connection avaible or the input source has been closed')
        line = self._inSource.readline()
        if line == "":
            return None
        
        student = StudentRecord()
        student.idNum = int( line )
        student.firstName = self._inSource.readline().rstrip()
        student.lastName = self._inSource.readline().rstrip()
        student.classCode = int(self._inSource.readline() )
        student.gpa = float( self._inSource.readline() ) 
        
        return student

    # new version
    def fetchRecord1( self ):
        if not self._inSource:
            raise ('No connection avaible or the input source has been closed')
        line = self._inSource.readline().split(self._separator)
   
        student = None
        if len(line) != 1:       
            student = StudentRecord()
            student.idNum = int( line[0] )
            temp = line[1].split(" ")
            student.firstName = temp[0].rstrip()
            student.lastName = temp[1].rstrip()
            student.classCode = int(line[2])
            student.gpa = float( line[3] )           
        return student

        
     #retrieve all records
    def fetchAll( self ):
        #studens records 
        studentsR = list()
        record = self.fetchRecord1() 
       
        while True:           
            if record != None :
                studentsR.append(record)
            else:
                break        
            record = self.fetchRecord1() 
        return studentsR

#Represent the student record ADT
class StudentRecord():

    def __init__( self ):
        self.idNum = 0
        self.firstName = None
        self.lastName = None
        self.classCode = 0
        self.gpa = 0.0

    
