#implements an ADT of student writer ouput
from student import studentReader
class StudentWriter:
    #constructs an writer
    def __init__( self, outputFile = None):
        self._outputFile = None if outputFile == None else outputFile
    
    #opens connection to the output source
    def open( self ):
        self._outputSource = open(self._outputFile, "w")

    #closes a connection from the output source
    def close( self ):
        self._outputSource.close()
        self._outputSource = None

    #writes a student record
    def write( self , studentRecord):
        if studentRecord == None:
            return None
            result = self.format_studentRecord(studentRecord)
            #no output file then print it out on the terminal
            if self._outputSource == None:
                print(result)
            else:
                #writes the content in the output source
                self._outputSource.write(result)

    #formats a given record to a specifique output format
    def format_studentRecord(self , record):
        return record

    #writes a whole student records
    def writeAll( self , theStudentsRecordsList):
        for record in theStudentsRecordsList:
            self.write(record)

if __name__ == "__main__":
    pass