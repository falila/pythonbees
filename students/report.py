#Student's report program that reads data from an input source and display them
from studentReader import StudentFileReader
from studentWriter import StudentWriter
import os

 #An input file name
SOURCEFILENAME = "studentsData.txt"
#main
def main():   
    print( os.getcwd() )
    os.chdir("D:\\python-workspace\\algorithms\\students\\")
    #reader = StudentFileReader("studentsData.txt")
    reader = StudentFileReader("studentsCSV.txt",",")
    #opens connection
    reader.open()
    studentList = reader.fetchAll()
    reader.close()

    #Sorting the student list
    studentList.sort(key = lambda reco: reco.idNum )

    #printing the report
    printReport( studentList )

def printReport( theStudentsList ):
        #class label
        classLabel = (None,"freshman","Sophomore","Junior","Senoir")
        #print the headers
        print("The student's report".center(50))
        print( "%-5s %-25s %-10s %-4s" % ('ID', 'NAME', 'CLASS', 'GPA' ) )
        print( "%5s %25s %10s %4s" % ('-' * 5, '-' * 25, '-' * 10, '-' * 4))
        # Print the body.
        for record in theStudentsList :
            print("%5d %-25s %-10s %4.2f" %(record.idNum, \
            str(record.lastName) + ', ' + str(record.firstName),
            classLabel[record.classCode], record.gpa) )
        # Add a footer.
        print( "-" * 50 )
        print( "Number of students:", len(theStudentsList) )

if __name__ == "__main__":main()