#
# Sorts a sequence in ascending order using the bubble sort algorithm.
def bubbleSort( theSeq ):
    n = len( theSeq )
    # Perform n-1 bubble operations on the sequence
    for i in range( n - 1 ) :
        # Bubble the largest item to the end.
        for j in range( i + n - 1 ) :
            if theSeq[j] > theSeq[j + 1] : # swap the j and j+1 items.
                tmp = theSeq[j]
                theSeq[j] = theSeq[j + 1]
                theSeq[j + 1] = tmp

#Implementation of the selection sort algorithm.
# Sorts a sequence in ascending order using the selection sort algorithm.
def selectionSort( theSeq ):
    n = len( theSeq )
    for i in range( n - 1 ):
        # Assume the ith element is the smallest.
        smallNdx = i
        # Determine if any other element contains a smaller value.
        for j in range( i + 1, n ):
            if theSeq[j] < theSeq[smallNdx] :
                smallNdx = j

    # Swap the ith value and smallNdx value only if the smallest value is
    # not already in its proper position. 
    if smallNdx != i :
        tmp = theSeq[i]
        theSeq[i] = theSeq[smallNdx]
        theSeq[smallNdx] = tmp

# Merges two sorted lists to create and return a new sorted list.
def mergeSortedLists( listA, listB ) :
    # Create the new list and initialize the list markers.
    newList = list()
    a = 0
    b = 0

    # Merge the two lists together until one is empty.
    while a < len( listA ) and b < len( listB ) :
        if listA[a] < listB[b] :
            newList.append( listA[a] )
            a += 1
        else :
            newList.append( listB[b] )
            b += 1

    # If listA contains more items, append them to newList.
    while a < len( listA ) :
        newList.append( listA[a] )
        a += 1

    # Or if listB contains more items, append them to newList.
    while b < len( listB ) :
        newList.append( listB[b] )
        b += 1
    return newList