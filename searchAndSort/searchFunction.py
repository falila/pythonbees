# In this section we are going to compare some search algorithms
#like linear search on an unsorted list or a sorted list ,  binary search.


#The linear search on an unsorted sequence
def linearSearch( theValues, target ) :
    n = len( theValues )
    for i in range( n ) :
        # If the target is in the ith element, return True
        if theValues[i] == target:
           return True

    return False # If not found, return False.

#The linear search on a sorted sequence
def sortedLinearSearch( theValues, item ) :
    n = len( theValues )
    for i in range( n ) :
        # If the target is found in the ith element, return True
        if theValues[i] == item :
            return True
            # If target is larger than the ith element, it's not in the sequence.
        elif theValues[i] > item :
            return False

    return False # The item is not in the sequence.

#The smallest value in an unsorted sequence.
def findSmallest( theValues ):
    n = len( theValues )
        # Assume the first item is the smallest value.
    smallest = theValues[0]
        # Determine if any other item in the sequence is smaller.
    for i in range( 1, n ) :
        if theValues[i] < smallest :
            smallest = theValues[i]

    return smallest # Return the smallest found.

#Binary search on a sorted list
def binarySearch( theValues, target ) :
    # Start with the entire sequence of elements.
    low = 0
    high = len(theValues) - 1

    # Repeatedly subdivide the sequence in half until the target is found.
    while low <= high :
        # Find the midpoint of the sequence.
        mid = (high + low) // 2
        # Does the midpoint contain the target?
        if theValues[mid] == target :
            return True
            # Or does the target precede the midpoint?
        elif target < theValues[mid] :
            high = mid - 1
            # Or does it follow the midpoint?
        else :
            low = mid + 1

        # If the sequence cannot be subdivided further, we're done.
    return False

#Unit test section.
