#implemets set ADT in python.
import test
class Set:
    #construts an empty set instance.
    def __init__( self ):
        self._elements = list()

    #Returns the number of items in the set.
    def __len__( self ):
        return len( self._elements )

    #Return true if an element is in the set.
    def __contains__( self, element ):
        return element in self._elements

    #Add a new element to the set.
    def add( self, element ):
        if element not in self :
            self._elements.append(element)
    
    #Removes an element from the set.
    def remove( self, element ):
        assert element in self, "The element must be in the set."
        self._elements.remove( element )

    # Determines if two sets are equal.
    def __eq__( self, setB):
        if len( self ) != len( setB ) :
            return False
        else:
            return self.isSubsetOf( setB )
    
    #computes if self is a subSet of setB
    def isSubsetOf( self, setB ):
        for element in self :
            if element not in setB :
                return False
        return True

    #create the union of this set and setB
    def union( self, setB ):
        newSet = Set()
        newSet._elements.extend( self._elements )
        for element in setB :
            newSet._elements.append( element )
        return newSet

    #creates a new set form the intersection self and setB.
    def interset( self, setB ):
        newSet = Set()
        for element in self :
            if element in setB :
                newSet._elements.append( element )
        return newSet

    #produces a new set from the difference : self and setB
    def difference( self, setB ):
        interSet = Set()
        for element in self:
            if element not in setB :
                interSet._elements.append( element )
        #looking for setB's elements that are not in self.
        for element in setB :
            if element not in self._elements :
                interSet._elements.append( element )
        return interSet
    
    #Returns an iterator for traversing the list of items
    def __iter__( self ):
        return _SetIterator( self._elements )

    #displays a set contains
    def display( self ):
        for element in self._elements :
            print(f"element {element}",flush=False)

    #Iterator 
 # This class represents a set iterator implementation that is used only inside a set class        
class _SetIterator:
    # constructs a set iterator with a given list
    def __init__( self, theList ):
        self._elemets = theList
        self._currentElement = 0

    # Returns the iterator himself
    def __iter__( self ):
        return self

    # Returns a next item from a set
    def __next__( self ):
        if self._currentElement < len( self._elemets ):
            item = self._elemets[ self._currentElement ]
            self._currentElement += 1
            return item
        else :
            raise StopIteration
    #unit testing 
def test_add():
    set1 = Set()
    #testing add function.
    set1.add(45)
    set1.add(3)
    set1.add(6)
    assert len (set1 ) == 3

def test_remove():
    set2 = Set()
    set2.add("2")
    set2.add("6")
    set2.add("hello")
    set2.remove("hello")
    set2.remove("2")
    for item in set2 :
        assert item != "hello"
        assert item != "2"

def test_union():
    data = ["one","two","three","four","five","six","seven"]
    set2 = Set()
    set2.add("one")
    set2.add("two")
    set2.add("three")
    set2.add("four")
    set3 = Set()
    set3.add("five")
    set3.add("six")
    set3.add("seven")
    union_set = set2.union(set3)
    
    for item in union_set :
        assert item in data 

    assert "eight" not in union_set

#tests intersect
def test_intersect():
    set2 = Set()
    set2.add("one")
    set2.add("two")
    set2.add("three")
    set2.add("four")
    set3 = Set()
    set3.add("one")
    set3.add("three")
    set3.add("seven")
    #must have one and three as values
    interSet = set2.interset(set3)
    assert len ( interSet ) == 2
    assert "one" in interSet
    assert "three" in interSet
    assert "two" not in interSet
    assert "four" not in interSet
    assert "seven" not in interSet

#tests intersect
def test_difference():
    set2 = Set()
    set2.add("one")
    set2.add("two")
    set2.add("three")
    set2.add("four")
    set3 = Set()
    set3.add("one")
    set3.add("three")
    set3.add("seven")
    #Intersert must contain four, two and seven as values.
    diffSet = set2.difference(set3)
    assert len ( diffSet ) == 3
    assert "one" not in diffSet
    assert "three"not in diffSet
    assert "two" in diffSet
    assert "four" in diffSet
    assert "seven" in diffSet


if __name__ == "__main__":
    pass