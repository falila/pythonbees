"""
This module is a customm implementation of Map ADT in python by using single list object
Like a dictionary, a map offers almost the same functionalities.
"""
class Map :
    #construts  an empty map instance
    def __init__( self ):
        self._entryList = list()

    #gives the number of entries in the map
    def __len__( self ):
        return len( self._entryList )

    #Determines if the map contains the given key.
    def __contains__( self, key ):
        index = self._findPosition( key )
        return index is not None
    
    #Adds a new entry to the map if the key does exist. Otherwise replaces the older one
    def add( self, key, value ) :
        index =  self._findPosition( key )
        if index is not None : # if the key has found
            self._entryList[index].value = value
            return False
        else : #otherwise add a new entry
            entry = _MapEntry( key, value )
            self._entryList.append( entry )
            return True
    
    #Returns the value associated with the key
    def valueOf( self, key ):
         index  = self._findPosition( key )
         assert index is not None, "Invalid map key."
         self._entryList[index].value

    #Removes the entry associated with the key
    def remove( self, key ):
        index = self._findPosition( key )
        assert index is not None, "Invalid map key."
        self._entryList.pop( index )

    #Returns an iterator for traversing the keys in the map.
    def __iter__( self ):
        return _MapIterator( self._entryList )

    #Helper function used to find the index position. if the 
    # key is not found, None is returned
    def _findPosition( self, key ):
        #iterator through each entry in the list
        for i in range( len(self) ):
            #is the key stored in the entry ?
            if self._entryList[i] == key :
                return i
            #when not found, return none.
        return None
    
    #Model class to store the key/value pairs.
class _MapEntry :
    def __init__( self, key, value ):
        self.key = key
        self.value = value

class _MapIterator :
    def __init__( self, theList):
        self._list = theList
        self._currentPosition = 0

    #returns a iterator that will be used to travers the map.
    def __iter__( self ):
        return self

    #Returns the next value 
    def __next__( self ):
        if self._currentPosition < len( self._list):
            key = self._list[self._currentPosition].key
            self._currentPosition += 1
            return key
        else : 
            raise StopIteration




