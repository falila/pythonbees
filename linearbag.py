# A custom implementation of the concept of a bag  ADT container in python
class Bag:
    #constructs an empty bag
    def __init__( self ):
        self._items = list()

    #Returns the number of items in the bag
    def __len__( self ):
        return len(self._items)

    #Determines if an items is contained in the bag
    def __contains__( self, item ):
        return item in self._items
    
    #Adds a new item to the bag
    def add( self, item ):
        self._items.append(item)

    #Removes and returns an instance of the item from the bag
    def remove( self, item ):
        assert item in self._items, "The must be in the bag."
        itemIndex = self._items.index( item )
        return self._items.pop( itemIndex )

    #return items
    def get_items( self ):
        return self._items    

    # Returns an iterator for traversing the list of items
    def __iter__( self ):
        return _BagIterator(self._items)
# This class represents a bag iterator implementation that is used inside a Bag class        
class _BagIterator:
    # constructs a bag iterator with a given list
    def __init__( self, theList ):
        self._bagItems = theList
        self._currentItem = 0

    # Returns the iterator himself
    def __iter__( self ):
        return self

    # Returns a next item from a bag
    def __next__( self ):
        if self._currentItem < len( self._bagItems ):
            item = self._bagItems[ self._currentItem ]
            self._currentItem += 1
            return item
        else :
            raise StopIteration

def main():
    # main function to run
    items = [item for item in range(15)]
    bag = Bag()
    for element in items:
        bag.add(element)
    for i in bag : print(f"item is  {i}")

    #test remove
    bag.remove(2)
    for i in bag : print(f"item is  {i}")

    #Counting bag
    

if __name__ == '__main__': main()
